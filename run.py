import subprocess
import time
import os
import signal

command = ["python3", "bot.py"]
games = 50
log = "game.log"

try:
    with open(log, "w") as l:
        l.seek(0)
        l.write("0")
        l.truncate()

    count = 1
    while count <= games:
        print("Running game n."+str(count))
        game = subprocess.Popen(command, shell=False, preexec_fn=os.setsid)
        while True:
            with open(log, "r+") as g:
                status = g.read().replace("\n", "")
                if status.startswith("1"):
                    os.killpg(os.getpgid(game.pid), signal.SIGTERM)
                    g.seek(0)
                    g.write("0")
                    g.truncate()
                    break

            time.sleep(1)

        print("Game n."+str(count)+" finished!")
        count += 1

except:
    game.terminate()
