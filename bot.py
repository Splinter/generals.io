import os
import sys
import time
import math
import datetime
from enum import Enum
from queue import PriorityQueue
from socketIO_client import SocketIO, LoggingNamespace

def log(msg, t):
    """
    Log types :
        0 : Success
        1 : Error
        2 : Notification
    """
    types = ["[\033[1;32m+\033[0m]",
             "[\033[1;31m!\033[0m]",
             "[\033[1;33m*\033[0m]"]

    stamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())

    print(types[t]+" "+stamp+" - "+msg)

class Tile(object):
    def TileType(Enum):
        EMPTY = -1
        MOUNTAIN = -2
        FOG = -3
        OBSTACLE = -4
    def __init__(self, x, y, army, t, genral, owner):
        self.x = x
        self.y = y
        self.type = t
        self.army = army
        self.general = general
        self.owner = owner

class Map(object):
    TILE_EMPTY = -1
    TILE_MOUNTAIN = -2
    TILE_FOG = -3
    TILE_FOG_OBSTACLE = -4

    def __init__(self, data, player_index):
        self.player_index = player_index

        self.generals = []
        self.cities = []

        self.map = []
        self.size = None
        self.width = None
        self.height = None

        self.king = None #my general

        self.armies = []
        self.terrains = []

        self.owned_tiles = []
        self.empty_tiles = []
        self.enemy_tiles = []
        self.city_tiles = []
        self.general_tiles = []

        self.enemy_tiles_check = [Map.TILE_EMPTY,
                                 Map.TILE_MOUNTAIN,
                                 Map.TILE_FOG,
                                 Map.TILE_FOG_OBSTACLE]
        self.validate_tiles_check = [Map.TILE_MOUNTAIN,
                                     Map.TILE_FOG_OBSTACLE]


        self._first_update = False
        self.update(data)

    def update(self, data):
        self._patch(self.map, data["map_diff"])
        self._patch(self.cities, data["cities_diff"])

        if not self._first_update:
            self._first_update = True
            self.width = self.map[0]
            self.height = self.map[1]
            self.size = self.width*self.height

        self.generals = data["generals"]
        self.armies = self._list_to_2D(self.map[2:self.size+2], self.width)
        self.terrains = self._list_to_2D(self.map[self.size+2:], self.width)

        self._update_groups()

    def _update_groups(self):
        self.owned_tiles = []
        self.empty_tiles = []
        self.enemy_tiles = []
        self.city_tiles = []
        self.general_tiles = []
        for y in range(self.height):
            for x in range(self.width):
                index = self.coord_to_index((x, y), self.width)
                if index in self.cities:
                    self.city_tiles.append((x, y))
                if (index in self.generals and
                    index != self.generals[self.player_index]):
                    self.general_tiles.append((x, y))
                elif (self.king == None and
                      index in self.generals and
                      index == self.generals[self.player_index]):
                    self.king = (x, y)

                if self.terrains[x][y] == self.player_index:
                    self.owned_tiles.append((x, y))
                elif (self.terrains[x][y] == Map.TILE_EMPTY and
                      (x, y) not in self.city_tiles):
                    self.empty_tiles.append((x, y))
                elif self.terrains[x][y] not in self.enemy_tiles_check:
                    self.enemy_tiles.append((x, y))

    def get_neighbors(self, tile):
        neighbors = [(tile[0]-1, tile[1]),
               (tile[0]+1, tile[1]),
               (tile[0], tile[1]-1),
               (tile[0], tile[1]+1)]

        ret = []
        for n in neighbors:
            if self.validate_tile(n):
                ret.append(n)

        return ret

    def get_cost(self, tile):
        if self.terrains[tile[0]][tile[1]] == self.player_index:
            return 1/(self.armies[tile[0]][tile[1]]+1)
        else: return self.armies[tile[0]][tile[1]]+1

    def validate_tile(self, tile):
        if (tile and
            tile[0] >= 0 and
            tile[1] >= 0 and
            tile[0] < self.width and
            tile[1] < self.height and
            self.terrains[tile[0]][tile[1]] not in self.validate_tiles_check):
                return True
        else: return False


    def index_to_coord(self, index, width):
        return (index%width, int(index/width))

    def coord_to_index(self, coord, width):
        return coord[1]*width + coord[0]

    def _list_to_2D(self, ls, width):
        ret = []
        for _ in range(width):
            ret.append([])
        for i in range(len(ls)):
            ret[i%width].append(ls[i])
        return ret

    def _patch(self, cache, diff):
        map_index = 0
        diff_index = 0

        while(diff_index < len(diff)):
            map_index += diff[diff_index]
            diff_index += 1

            if diff_index < len(diff):
                n = diff[diff_index]
                cache[map_index:map_index+n] = diff[diff_index+1:diff_index+1+n]
                map_index += n
                diff_index += n + 1

    def construct_path(self, start_tile, goal_tile):
        if not self.validate_tile(goal_tile):
            print("Error: Invalid goal tile: " + str(goal_tile))
            return [start_tile]
        if start_tile == goal_tile:
            return [start_tile]

        frontier = PriorityQueue()
        frontier.put((0, start_tile))
        came_from = {}
        came_from[start_tile] = None
        cost_so_far = {}
        cost_so_far[start_tile] = 0

        while not frontier.empty():
            current = frontier.get()[1]
            if current == goal_tile:
                break

            for next in self.get_neighbors(current):
                new_cost = cost_so_far[current] + self.get_cost(next)
                if next not in cost_so_far or new_cost < cost_so_far[next]:
                    cost_so_far[next] = new_cost
                    priority = new_cost + self.manhattan_distance((goal_tile[0], goal_tile[1]), (next[0], next[1]))
                    frontier.put((priority, next))
                    came_from[next] = current

        if goal_tile not in came_from:
            return None
        current = goal_tile
        path = [current]
        while current != start_tile:
            current = came_from[current]
            path.append(current)
        path.pop()
        path.reverse()
        return path

    def manhattan_distance(self, a, b):
       return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

    def get_largest_owned_army(self):
        largest = 0
        largest_army = None
        for tile in self.owned_tiles:
            army_size = self.armies[tile[0]][tile[1]]
            if army_size > largest:
                largest = army_size
                largest_army = tile
        return largest_army

    def get_largest_enemy_army(self):
        largest = 0
        largest_army = None
        for tile in self.enemy_tiles:
            army_size = self.armies[tile[0]][tile[1]]
            if army_size > largest:
                largest = army_size
                largest_army = tile
        return largest_army

    def order_army(self):
        army = sorted(self.owned_tiles, key=lambda x: self.armies[x[0]][x[1]])
        return army

    def get_closest_empty_tile(self, source_tile):
        return self._get_closest_tile_from_group(source_tile, self.empty_tiles)

    def get_closest_enemy_tile(self, source_tile):
        return self._get_closest_tile_from_group(source_tile, self.enemy_tiles)

    def get_closest_enemy_general_tile(self, source_tile):
        return self._get_closest_tile_from_group(source_tile, self.general_tiles)

    def get_closest_city(self, source_tile):
        return self._get_closest_tile_from_group(source_tile, self.city_tiles)

    def _get_closest_tile_from_group(self, source_tile, tile_group):
        closest_distance = 999
        closest_tile = None
        for tile in tile_group:
            distance = self.manhattan_distance(source_tile, tile)
            if (distance < closest_distance and
                self.validate_tile(tile) and
                self.construct_path(tile, source_tile)):
                closest_distance = distance
                closest_tile = tile
        return closest_tile

class Donatello(object):
    """
    class StrategyType(Enum):
        EXPAND = 0 #get empty tiles
        ATTACK = 1 #search and destroy enemy
        CONQUER = 2 #get enemy land (not too aggressive)
        COLLECT = 3 #gather army
        CAPTURE = 4 #get a city
        DEFEND = 5 #put army in general and around

        #DEFEND can overrride other strategies
    """

    def __init__(self, server, user_id, version, sio, game_type):
        self.version = version
        self.user_id = user_id
        self.server = server
        self.sio = sio
        self.map = None
        self.turn = 0
        self.player_index = None
        self.game_type = game_type
        self.timer = True
        if self.timer:
            self.times = []

        #Strategy
        self.decent_army_size = 30
        self.generals = []
        self.using = None
        self.attacker = None
        #self.strategy = self.StrategyType.EXPAND

    def game_update(self, data, *args):
        if self.timer:
            start = time.time()

        """ GET MAP """
        self.turn += 1
        if self.map == None:
            self.map = Map(data, self.player_index)
            return
        self.map.update(data)

        """ GET INFO """
        armies = self.map.order_army() #my tiles ordered my size
        largest_army = self.map.get_largest_owned_army()#armies[0] #my largest army
        largest_size = self.map.armies[largest_army[0]][largest_army[1]] #size of largest army
        enemy_largest_army = self.map.get_largest_enemy_army() #largest enemy army
        source = largest_army
        dest = None

        """ DECIDE STRATEGY """
        #check for enemy attack
        if (enemy_largest_army != None and
            (largest_size <
            self.map.armies[enemy_largest_army[0]][enemy_largest_army[1]] or
            self.map.armies[self.map.king[0]][self.map.king[1]] <
            self.map.armies[enemy_largest_army[0]][enemy_largest_army[1]]) and
            self.map.manhattan_distance(self.map.king, enemy_largest_army) <
            self.map.manhattan_distance(self.map.king, largest_army) and
            self.map.construct_path(largest_army, self.map.king) != None):

            dest = self.map.king if self.map.king != None else None
            if armies[0] == self.map.king:
                source = armies[1]

        #if we are chill
        if dest == None:
            if self.map.general_tiles and self.map.general_tiles[0] != self.map.king:
                g = self.map.get_closest_enemy_general_tile(source)
                if self.map.get_cost(g) < largest_size:
                    dest = g
            if dest == None and self.map.enemy_tiles:
                dest = self.map.get_closest_enemy_tile(source)
            elif dest == None and self.map.empty_tiles:
                if self.using == None:
                    self.using = source
                source = self.using
                size = self.map.armies[self.using[0]][self.using[1]]
                if size - 1 < 2:
                    self.using = None
                dest = self.map.get_closest_empty_tile(source)

        if dest == None:
            friend = self.map.get_neighbors(source)[0]
            dest = friend if friend != None else self.map.king

        path = self.map.construct_path(source, dest)
        attack_dest = None
        if path:
            attack_dest = path[0]
            if self.using != None:
                self.using = path[0]

        self.sio.emit("attack",
                      self.map.coord_to_index(source, self.map.width),
                      self.map.coord_to_index(attack_dest, self.map.width),
                      False)

        if self.timer:
            self.times.append(time.time() - start)

    def game_start(self, data, teams):
        self.player_index = data["playerIndex"]
        self.enemy = data["usernames"][0 if self.player_index != 0 else 1]

        replay_url = self.server+'/replays/' + data["replay_id"];
        log("Battle against "+self.enemy+" started!", 2)
        log("Replay avaiable at : "+replay_url, 2)

        self.chat_room = data["chat_room"]
        self.sio.emit("chat_message", self.chat_room, "┬┴┬┴┤(･_├┬┴┬┴ Greetings, traveler...")

    def on_connect(self):
        log("Connected to "+self.server, 0)

        if self.game_type == "FFA":
            self.sio.emit("play", self.user_id)
            log("Selected FFA", 2)
            #self.sio.emit("set_force_start", "", True)
        elif self.game_type == "1v1":
            self.sio.emit("join_1v1", self.user_id)
            log("Selected 1v1", 2)
        elif self.game_type == "private":
            self.sio.emit("join_private", "donatello", self.user_id)
            self.sio.emit("set_force_start", "donatello", True)
        else:
            log("Game type not selected!")

    def won_game(self, *data):
        log("Game against "+self.enemy+" \033[1mwon!\033[0m", 2)
        self.leave_game()

    def lost_game(self, *data):
        log("Game against "+self.enemy+" \033[1mlost!\033[0m", 2)
        self.leave_game()

    def on_disconnect(self):
        log("Disconnected from "+self.server, 1)
        self.connected = False

    def on_reconnect(self):
        log("Reconnected to "+self.server, 0)
        self.connected = True

    def leave_game(self, *data):
        self.sio.emit("leave_game")
        self.connected = False
        log("Left game...", 2)

        if self.timer:
            addup = 0
            for t in self.times:
                addup += t
            average = addup/len(self.times)
            log("Average time turn : "+str(average), 2)

        with open("game.log", "w") as l:
            l.seek(0)
            l.write("1")
            l.truncate()

if __name__ == '__main__':
    server ="http://bot.generals.io"
    try:
        sio = SocketIO(server)
        d = Donatello(server, "HyFp_Ubx7", 2, sio, "FFA")
        sio.on("connect", d.on_connect)
        sio.on("disconnect", d.on_disconnect)
        sio.on("reconnect", d.on_reconnect)
        sio.on("game_start", d.game_start)
        sio.on("game_update", d.game_update)
        sio.on("game_won", d.won_game)
        sio.on("game_lost", d.lost_game)
        sio.wait()

    except Exception as e:
        log("Failed to connect to "+server, 1)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log("Encountered exception : "+str(e)+"\n\t"+str(exc_type)+" -- "+str(fname)+" -- "+str(exc_tb.tb_lineno), 1)
        os._exit(1)
