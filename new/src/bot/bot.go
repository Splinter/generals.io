package bot

import (
	"math/rand"
	"os"
	"time"

	"github.com/Splinter0/gioframework"
)

func InitBot(id, name, g string, games int) {
	c, err := gioframework.Connect("bot", id, name)
	if err != nil {
		SuperLog("Failed to connect to server!", 1)
		os.Exit(1)
	}
	SuperLog("Successfully connected to server!", 0)
	go c.Run()

	for i := 0; i < games; i++ {
		var game *gioframework.Game
		if g == "ffa" {
			game = c.JoinClassic()
			SuperLog("Selected FFA", 2)
		} else if g == "1v1" {
			game = c.Join1v1()
			SuperLog("Selected 1v1", 2)
			game.SetForceStart(true)
		} else {
			game = c.JoinCustomGame(g)
			SuperLog("Selected private game : "+g, 2)
			game.SetForceStart(true)
		}

		started := false
		game.Start = func(playerIndex int, users []string, replay string) {
			var players string
			for u := range users {
				if users[u] != name {
					players += users[u] + " "
				}
			}
			SuperLog("Game against : "+players, 1)
			SuperLog("Replay avaiable at http://bot.generals.io/replays/"+replay, 2)
			started = true
		}
		done := false
		game.Won = func() {
			SuperLog("Won game!", 0)
			done = true
		}
		game.Lost = func() {
			SuperLog("Lost game...", 1)
			done = true
		}
		for !started {
			time.Sleep(1 * time.Second)
		}

		time.Sleep(1 * time.Second)

		for !done {
			time.Sleep(100 * time.Millisecond)
			if game.QueueLength() > 0 {
				continue
			}
			mine := []int{}
			for i, tile := range game.GameMap {
				if tile.Faction == game.PlayerIndex && tile.Armies > 1 {
					mine = append(mine, i)
				}
			}
			if len(mine) == 0 {
				continue
			}
			cell := rand.Intn(len(mine))
			move := []int{}
			for _, adjacent := range game.GetAdjacents(mine[cell]) {
				if game.Walkable(adjacent) {
					move = append(move, adjacent)
				}
			}
			if len(move) == 0 {
				continue
			}
			movecell := rand.Intn(len(move))
			game.Attack(mine[cell], move[movecell], false)

		}
	}
}
