package bot

import (
	"fmt"
	"time"
)

func SuperLog(msg string, t int) {
	var types = [...]string{
		"[\033[1;32m+\033[0m]", //Success 0
		"[\033[1;31m!\033[0m]", //Error 1
		"[\033[1;33m*\033[0m]", //Notification 2
	}
	stamp := time.Now().Format("2006-01/02/-15:04:05")
	fmt.Println(types[t] + " " + stamp + " - " + msg)
}
